package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("FAULT!!! Command Not Supported");
    }

    public CommandNotSupportedException(final String command) {
        super("FAULT!!!" + command + " is not supported!");
    }

}
