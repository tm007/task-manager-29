package ru.tsc.apozdnov.tm.exception.user;

public class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("Error!!! UserId is empty !");
    }

}
