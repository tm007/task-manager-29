package ru.tsc.apozdnov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("FAULT! Index is EMPTY!!!");
    }

}
