package ru.tsc.apozdnov.tm.exception.user;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}