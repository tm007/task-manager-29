package ru.tsc.apozdnov.tm.exception.field;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
        super();
    }

    public AbstractFieldException(final String message) {
        super(message);
    }

    public AbstractFieldException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
