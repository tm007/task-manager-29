package ru.tsc.apozdnov.tm.exception.user;

public class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error!!! User not found !");
    }

}
