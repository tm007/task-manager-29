package ru.tsc.apozdnov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.component.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final ISaltProvider iSaltProvider,
            @Nullable final String value
    ) {
        if (iSaltProvider == null) return null;
        @NotNull final String passwordSecret = iSaltProvider.getPasswordSecret();
        @NotNull final Integer passwordIterator = iSaltProvider.getPasswordIteration();
        return salt(value, passwordSecret, passwordIterator);
    }

    @NotNull
    static String salt(final String value, @Nullable final String secret, @Nullable final Integer iteration) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(final String value) {
        if (value == null) return null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
