package ru.tsc.apozdnov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "dd.MM.yyyy";

    @NotNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    static Date toDate(final String date) {
        try {
            return FORMATTER.parse(date);
        } catch (final ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @NotNull
    static String toString(final Date date) {
        if (date == null) return "";
        return FORMATTER.format(date);
    }

}
