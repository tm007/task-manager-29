package ru.tsc.apozdnov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.command.data.backup.DataBackupLoadCommand;
import ru.tsc.apozdnov.tm.command.data.backup.DataBackupSaveCommand;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    private void load() {
        bootstrap.processCommandTask(DataBackupLoadCommand.NAME, false);
    }

    private void save() {
        bootstrap.processCommandTask(DataBackupSaveCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(2000);
            save();
        }
    }

}