package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** SHOW PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
